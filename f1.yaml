#################################################
patient_header:
    section: Patient Info
#################################################


study_id:
    label: Study ID
    type: number
    definition: |-
        Study ID is a unique number assigned to each participant according to the center of origination. The first variable for each center has been entered, but future study ID's should be entered by each center.  Per the current IRB protocol, this database will not contain PHI and each center is responsible for maintaining a separate database that links the Study ID to the appropriate patient.
        Newark Beth Israel ..............1100001
        Colorado...............................1200001
        Columbia..............................1300001
        Duke.....................................1400001
        Minnesota Heart..................1500001
        Washington University.........1600001
        Westchester.........................1700001

second_ecmo:
    label: Was this a Second run of ECMO?
    radio:
        - 0:  Yes
        - 1:  ~No
    definition: |-
        For instances where the patient is decannulated but then subsequently recannulated within the same hospital visit, the subsequent ECMO recannulations will be entered into the database as new entries with new Study IDs. A value of "No" indicates that this entry corresponds to the first, and possibly only, run of ECMO for this patient. Any subsequent runs of ECMO will have a value of "Yes".

        A "yes" value in this field will also automatically omit Demographic, Pre-hospital characteristics, and Admissions questions from the user since these values are not expected to change between the different runs of ECMO. Since Outcomes and Complications are difficult to attribute to any one specific run of ECMO, those questions will also be omitted from redundant entry.

study_id_from_first_ecmo:
    label: Study ID from First ECMO
    type: number
    logic: >-
        second_ecmo = "Yes"
    definition: |-
        This field contains the study ID from the participant's first ECMO procedure during this hospitalization. This field should be ignored if 2. Second ECMO is answered "No".

study_design:
    label: Study Design
    radio:
        - 0:  Prospective
        - 1:  Retrospective
    definition: |-
        The arm of the study for which the patient is being enrolled, whether prospective or retrospective. Marking this field with a value of "Retrospective" will automatically omit certain fields from data entry. This behavior was added to fields for which obtaining the patient data retrospectively would prove very difficult, if not impossible.

ecmo_mode:
    label: Initial ECMO Mode
    radio:
        - 0:  V-V ECMO
        - 1:  V-A ECMO
    definition: |-
        The initial mode of ECMO.
        V-V ECMO takes blood from a large vein and returns oxygenated blood back to a large vein. VV ECMO does not provide hemodynamic support to the circulation.
        V-A ECMO takes deoxygenated blood from a central vein or from the right atrium, pumps it past the oxygenator and then returns the oxygenated blood, under pressure, to the arterial side of the circulation.
        ECMO initiated in V-A-V configuration, is consider V-A ECMO for this question.


#################################################
demographics_header:
    section: Demographics Information
#################################################


age:
    label: Age at ECMO Initiation
    type: number
    note: years
    definition: |-
        The patient's age in years, at time of ECMO initiation. This should be calculated from the date of birth and the date of ECMO, according to the convention used in the USA (the number of birthdate anniversaries reached by the date of ECMO).  This is an adult registry therefore no patient under the age of 18 should be entered.

sex:
    label: Gender
    radio:
        - 0:  Female
        - 1:  Male
    identifier: TRUE
    definition: |-
        The participant's gender at birth, since patients who have undergone gender reassignment surgery maintain many of the risks associated with their chromosomal gender.

race_ethnicity:
    label: Race/Ethnicity
    radio:
        - 0:  African-American or Black
        - 1:  Asian
        - 2:  White
        - 3:  Hawaiian or Native Pacific Islander
        - 4:  Hispanic/Latino
        - 5:  Native American or Alaska Native
        - 6:  **Other**
    definition: |-
        Race should be self‐reported by the patient/family.  Do not assign race or make assumptions if race is not documented.

zip:
    label: ZIP
    type: text
    identifier: TRUE
    definition: |-
        The ZIP Code of the patient's local residence. Outside the USA, this data may be known by other names such as Postal Code.

weight:
    label: Weight at ECMO Implantation
    type: number
    note: Kg
    definition: |-
        The weight of the patient, in kilograms, closest to the date of procedure.

height:
    label: Height at ECMO Implantation
    type: number
    note: cm
    definition: |-
        The height of the patient, in centimeters, closest to the date of the procedure.

bmi:
    label: BMI
    formula: >-
        round([weight]/(([height]/100*[height]/100)),1)
    definition: |-
        The body mass index, auto-calculated using the formula below and rounded to the nearest tenth.

bsa:
    label: BSA
    formula: >-
        sqrt([height]*[weight]/3600)
    definition: |-
        The body surface area, auto-calculated using the Mosteller formula


#################################################
admission_header:
    section: Admission Information
#################################################


hosp_admit_date:
    label: Hospital Admission Date
    type: date_mdy
    identifier: TRUE
    definition: |-
        The date the patient arrived at the facility. For patients who originally enter the hospital in an out‐patient capacity (i.e., catheterization), the admit date is the date the patient's status changes to in‐patient.

hosp_dx:
    label: Hospital Primary Admitting Diagnosis
    checkbox:
        - 0:  Acute Coronary Syndrome
        - 1:  Cardiac Surgery (not transplant)
        - 2:  Cardiomyopathy - Ischemic
        - 3:  Cardiomyopathy - Non-ischemic
        - 4:  Respiratory Failure - Hypercarbia
        - 5:  Respiratory Failure - Hypoxemia
        - 6:  Transplantation - Lung
        - 7:  Transplantation - Heart
        - 8:  **Other**
    logic: >-
        study_design = "Prospective"
    definition: |-
        The primary diagnosis for which the patient was admitted into the hospital, as specified by earliest available H & P.

icu_admit_date:
    label: ICU admission date
    type: date_mdy
    identifier: TRUE
    definition: |-
        The date the patient was admitted to the intensive care unit during the index hospitalization.

hosp_icu_dx_match:
    label: >-
        Are the hospital and ICU primary diagnosis the same?
    radio:
        - 0:  Yes
        - 1:  ~No
    logic: >-
        study_design = "Prospective"
    definition: |-
        Indicates whether the primary diagnosis for admission into the hospital was the same as the diagnosis  for admission into the ICU.

icu_dx:
    label: ICU Primary Diagnosis
    radio:
        - 0:  Acute Coronary Syndrome
        - 1:  Cardiac Surgery (not transplant)
        - 2:  Cardiomyopathy - Ischemic
        - 3:  Cardiomyopathy - Non-ischemic
        - 4:  Respiratory Failure - Hypercarbia
        - 5:  Respiratory Failure - Hypoxemia
        - 6:  Transplantation - Lung
        - 7:  Transplantation - Heart
        - 8:  **Other**
    logic: >-
        hosp_icu_dx_match = "No"
    definition: |-
        If the ICU Diagnosis is different than the hospital diagnosis please indicate this diagnosis in this field.
