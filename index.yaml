import:
  - f1.yaml
  - f2.yaml
  - f3.yaml
  - f4.yaml
  - metrics.yaml
  - outcomes.yaml
  - complications.yaml


include:
    - form_1
    - form_2
    - form_3
    - form_4
    - form_5
    - form_6
    - form_7
    - form_8
    - form_9
    - form_10
    - form_11


#################################################
form_1:
  form: Demographic Admission Information
  include:
    - patient_section
    - demographics_section
    - admission_section


patient_section:
  include:
    - patient_header
    - study_id
    - second_ecmo
    - study_id_from_first_ecmo
    - study_design
    - ecmo_mode

demographics_section:
  include:
    - demographics_header
    - age
    - sex
    - race_ethnicity
    - zip
    - weight
    - height
    - bmi
    - bsa

admission_section:
  include:
    - admission_header
    - hosp_admit_date
    - hosp_dx
    - icu_admit_date
    - hosp_icu_dx_match
    - icu_dx

#################################################

form_2:
  form: Prehospital Baseline Characteristics
  include:
    - disease_section
    - substances_section


substances_section:
  include:
    - substances_header
    - tobacco_use
    - pack_years
    - alcohol_use
    - drug_use
    - drug_type

disease_section:
  include:
    - hx_disease_header
    - preexisting_cardiac_dz
    - preexisting_lung_dz
    - past_cts
    - past_medical_hx
    - past_organ_transpl
    - active_cancer
    - immunosuppression

#################################################
form_3:
  form: Indication Preecmo Illness Severity
  include:
    - indication_section
    - post_cardiotomy_section
    - cardiac_arrest_section
    - mcs_section


indication_section:
  include:
    - indication_header
    - indication_ecmo
    - etiology_shock
    - mi_treat
    - resp_fail_heart_fail

post_cardiotomy_section:
  include:
    - post_cardiotomy_header
    - cts_pre_ecmo
    - pcs_cpb_time
    - pcs_cross_clamp_time

cardiac_arrest_section:
  include:
    - cardiac_arrest_header
    - witnessed_arrest
    - cpr
    - cpr_external_type
    - cpr_mech_device_name
    - cpr_duration_in_minutes
    - rosc_pre_ecmo_deploy
    - init_hypothermia
    - hypothermia_lowest_temp
    - duration_hypothermia

mcs_section:
  include:
    - mcs_header
    - mcs_pre_ecmo

#################################################
form_4:
  form: Characteristics Technical Information
  include:
    - gen_info_section
    - ecmo_mode_section
    - cannula_technical_info_section
    - lv_vent_section
    - distal_perfusion_section
    - circuit_section


gen_info_section:
  include:
    - gen_info_header
    - ecmo_init_date_time
    - ecmo_discontinuation_date_time
    - ecmo_los
    - cann_physician_specialty
    - cann_loc

ecmo_mode_section:
  include:
    - ecmo_mode_header
    - ecmo_initial_mode
    - ecmo_conversion_group    # replicates 5 times

cannula_technical_info_section:
  include:
    - cannula_technical_info_header
    - arterial_access_group     # replicates 3x
    - venous_access_group     # replicates 3x, also

lv_vent_section:
  include:
    - lv_vent_header
    - lv_vent
    - lv_vent_route
    - lv_vent_method
    - lv_vent_reason
    - lv_vent_cann
    - lv_vent_date

distal_perfusion_section:
  include:
    - dist_perf_cann
    - dist_perf_cann_time_initial_cann
    - dist_perf_cann_date
    - dist_perf_cann_indication
    - dist_perf_cann_loc
    - dist_perf_cann_size

circuit_section:
  include:
    - circuit_coating
    - oxygenator
    - pump


ecmo_conversion_group:
    replicate: 5
    include:
        - ecmo_conv_type
        - ecmo_conv_date

arterial_access_group:
    include:
        - art_acc
        - art_acc_cann_size
        - art_acc_graft_anast
        - art_acc_graft_anast_size
    replicate: 3

venous_access_group:
    include:
        - ven_acc
        - ven_acc_cann_size
    replicate: 3


#################################################
form_11:
  form: Outcomes
  include:
    - mortality_outcomes_section
    - icu_outcomes_section
    - hospital_outcomes_section
    - ecmo_procedures_section
    - pulmonary_complications_section
    - blood_complications_section
    - neuro_complications_section
    - renal_complications_section
    - infectious_complications_section
    - circuit_complications_section
    - other_complications_section
    - oxygenator_section



mortality_outcomes_section:
  include:
    - mortality_outcomes_header
    - patient_decann
    - decann_date
    - status
    - status_date
    - death_during_hosp
    - cause_death
    - timing_withdrawal
    - reason_withdrawal

icu_outcomes_section:
  include:
    - icu_outcomes_header
    - best_ambul_cond
    - out_bed_date
    - header_discharge
    - icu_discharge_date
    - icu_discharge_loc
    - first_icu_los
    - icu_readmit
    - icu_readmit_reason

hospital_outcomes_section:
  include:
    - hosp_discharge_date
    - hosp_discharge_loc
    - hosp_los
    - hosp_readmit_within_30_days
    - hosp_readmit_reason

ecmo_procedures_section:
  include:
    - ecmo_procedures_header
    - ecmo_procedures_group    # replicates 5x
    - decann_tools

# Complications

pulmonary_complications_section:
  include:
    - pulmonary_cmp_header
    - chest_related_cmp
    - chest_related_cmp_date
    - trach
    - trach_date
    - ventilator_ext_date
    - ext_pre_decann
    - re_intubation
    - re_intubation_date
    - pulm_outcomes
    - discharge_lung_metrics_group

blood_complications_section:
  include:
    - blood_cmp_header
    - vasc_cmp_type
    - vasc_cmp_date
    - vasc_cmp_treat_bleed
    - vasc_cmp_treat_hyperperf
    - vasc_cmp_treat_hypoperf
    - thrombotic_cmp
    - thrombotic_cmp_date
    - lv_thrombus
    - lv_thrombus_date
    - circuit_related_hemolysis_ldh
    - bleeding_event_group          # replicates 5x

neuro_complications_section:
  include:
    - neuro_header
    - neuro_cmp
    - neuro_cmp_date


renal_complications_section:
  include:
    - renal_cmp_header
    - reminder_chronic_dialysis
    - renal_cmp
    - renal_cmp_date
    - cre_discharge
    - dialysis_discharge

infectious_complications_section:
  include:
    - infectious_cmp_header
    - infections_group    # replicates 5x


circuit_complications_section:
  include:
    - circuit_cmp_header
    - circuit_related_technical_cmp
    - device_malfunction_date
    - device_malfunction_cause


other_complications_section:
  include:
    - other_complications_header
    - otr_cmp

oxygenator_section:
  include:
    - oxygenator_header
    - oxygenator_exchange_group




ecmo_procedures_group:
    include:
        - ecmo_cardiac_proc
        - ecmo_cardiac_proc_date
    replicate: 5

discharge_lung_metrics_group:
    include:
        - spo2
        - fio2
        - s_f_ratio
    $$suffix: _discharge
    $$when: Discharge
    logic: >-
        pulm_outcomes = "Oxygen therapy at time of discharge"

bleeding_event_group:
    replicate: 5
    include:
        - bleedev_src
        - bleedev_date
        - bleedev_inr
        - bleedev_reop
        - bleedev_transfusion
        - bleedev_transfusion_amount
        - bleedev_anticoag
        - bleedev_resolution

infections_group:
  replicate: 5
  include:
    - infect_site
    - infect_micro
    - infect_date

oxygenator_exchange_group:
  replicate: 5
  include:
    - oxor_ex_reason
    - oxor_ex_date
    - oxor_ex_type


#################################################
# Here's where the craziness really starts, the middle 6
# forms. I tried to group with as little redundancy as possible
# however, so please excuse the weirdness.
#
#
#
# Most metrics require the following variables:
#   when - The timepoint in plain english.
#             e.g., "Pre-ECMO", "During ECMO", "0 H",...
#   suffix - The timepoint's abbreviation,
#             which will be appended to the variable name.
#             e.g., "_pre", "", "post", "0h", "24h",...
#################################################

form_5:
  $when: Pre-ECMO
  $suffix: _pre
  include:
    - ventilator
    - ventilator_init_date
    - vital_signs_section
    - glasgow_comma_score
    - drugs_vaso_ino
      # verify that right vent is referenced
    - ventilator2
    - ventilator_mode
    - ventilation_section
    - oxidx
    - bmp_pre_section
    - iabp
    - echo_section
    - cath_date
    - cardiac_section


form_6:
  $when: Hour 24
  $suffix: _24h
  include:
    - flow_0h
    - flow_4h
    - flow
    #- ventilator
    #- ventilator_init_date
    - vital_signs_section
    #- glasgow_comma_score
    - blood_products_section
    - drugs_vaso_ino
    #- vent_and_mode
    - ventilation_section
    - oxidx
    - bmp_section
    #- iabp
    #- echo_section
    #- cath_date
    - cardiac_section


form_7:
  $when: Hour 48
  $suffix: _48h
  include:
    - flow
    - vital_signs_section
    #- blood_products_section
    - drugs_vaso_ino
    - ventilation_section
    #- oxidx
    - bmp_section
    - cardiac_section

form_8:
  $when: Hour 72
  $suffix: _72h
  include:
    - flow
    - vital_signs_section
    - drugs_vaso_ino
    - ventilation_section
    - bmp_section
    - cardiac_section


form_9:
  $when: During ECMO
  $suffix: ""
  include:
    - flow_avg
    - flow_index
    - vital_signs_section
    #- glasgow_comma_score
    - blood_products_section
    - blood_products_extension
    - drugs_vaso_ino
    - drugs_extension
    - special_labs_section
    #- vent_and_mode
    #- ventilation_section
    #- bmp_section
    #- iabp
    - echo_section
    #- cath_date
    #- cardiac_section
    - bmp_minimum_labs
    - bmp_maximum_labs

form_10:
  # todo: finish this
  $when: Post-ECMO
  $suffix: '_post'
  include:
    - echo_section
    - serial_echos_post
    - cath_date
    - cardiac_section

vital_signs_section:
  include:
    - vital_signs_header
    - temperature
    - mean_arterial_pressure
    - heart_rate
    - respiratory_rate



blood_products_section:
  include:
    - blood_products_header
    - prbc
    - ffp
    - plt
    - cryop

blood_products_extension:
  include:
    - factor_7
    - factor_7_dose
    - factor_7_initiated
    - pcc
    - pcc_dose
    - pcc_initiated

drugs_vaso_ino:
  include:
    - drugs_header
    - vasopres
    - inotropes

drugs_extension:
  include:
    - drugs_header
    - vasodil
    - antibiotics
    - neuroblock
    - sedatives
    - antifibrin
    - anticoagulation_group


special_labs_section:
  include:
    - special_labs_header
    - hit_antibody
    - sra
    - bnp

ventilation_section:
  include:
    - pao2
    - fio2
    - p_f_ratio
    - paco2
    - pip
    - mean_airway_press
    - peep
    - tidal_vol
    # don't forget to include oxidx for pre, 24h

bmp_pre_section:
  include:
    - bmp_date
    - sodium
    - bmp_section




bmp_section:
  include:
    - ph
    - urineout
    - bun
    - bicarb
    - lactate
    - cre
    - glu
    - tot_protein
    - albumin
    - tbili
    - ast
    - alt
    - alk_phos
    - ldh
    - ptt
    - inr
    - fibrinogen

bmp_minimum_labs:
  $when: Minimum
  $suffix: _min
  include:
    - select_header
    - ptt
    - ptt_date

bmp_maximum_labs:
  $when: Maxiumum
  $suffix: _max
  include:
    - select_header
    - bun
    - bun_date
    - lactate
    - lactate_date
    - cre
    - cre_date
    - tbili
    - tbili_date
    - ast
    - ast_date
    - alt
    - alt_date
    - ldh
    - ldh_date
    - ptt
    - ptt_date
    - inr
    - inr_date
    - peak_trop_max
    - peak_trop_date
    - tot_cre_kinase_max
    - tot_cre_kinase_date
    - cre_kinase_mb_max
    - cre_kinase_mb_date



echo_section:
  include:
    - echo_header
    - echo
    - echo_date
    - lvedd
    - lvesd
    - ef
    - ai
    - as
    - mr
    - ms
    - tr


# when, suffix

cardiac_section:
  include:
    - rhythm
    - sbp
    - dbp
    - pulm_sbp
    - pulm_dbp
    - pulm_map
    - pcwp
    - cvp_rap
    - svo2
    - co

anticoagulation_group:
    include:
        - anticoag
        - anticoag_type
        - anticoag_init_time
        - anticoag_end_time
        - anticoag_duration

vent_and_date:
    include:
        - ventilator
        - ventilator_init_date

vent_and_mode:
    include:
        - ventilator2
        - ventilator_mode
