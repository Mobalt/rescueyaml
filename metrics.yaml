##################################################
# Flow
##################################################

flow_0h:
  $when: ECMO Initiation
  $suffix: _0h
  include: flow

flow_4h:
  $when: 4 H
  $suffix: _4h
  include: flow

flow:
    name: "flow{{suffix}}"
    label: "ECMO Flow at {{when}}"
    type: number
    note: L/min
    definition: |-
        The blood flow through the ECMO circuit.

flow_avg:
    type: calc
    label: Avg. Flow
    formula: >-
        ([flow_0h] + [flow_4h] + [flow_24h] + [flow_48h] + [flow_72h])/ 5
    note: L/min
    definition: |-
        The auto-calculated average blood flow through the ECMO circuit.

flow_index:
    label: Flow Index
    type: number
    definition: |-
        The blood flow index through the ECMO circuit.

glasgow_comma_score:
    name: "glasgow_comma{{suffix}}"
    label: "{{when}} Glasgow Comma Score (GCS)"
    type: number
    definition: |-
        The Glasgow Comma Score (GCS) is a neurological scale that measures the patient's level of consciousness.

#################################################
vital_signs_header:
    section: >-
        {{when}} Vital Signs
#################################################

temperature:
    name: "temp{{suffix}}"
    label: "{{when}} Temperature"
    note: Celsius
    type: number
    definition: |-
        A measure of the core body temperature.

mean_arterial_pressure:
    name: "map{{suffix}}"
    label: "{{when}} Mean Arterial Pressure"
    note: mmHg
    type: number
    definition: |-
        A measure of the Mean Arterial Pressure (MAP).

heart_rate:
    name: "hr{{suffix}}"
    label: "{{when}} HR"
    note: bpm
    type: number
    definition: |-
        A measure of the heart rate.

respiratory_rate:
    name: "rr{{suffix}}"
    label: "{{when}} RR"
    note: breaths/min
    type: number
    logic: >-
        study_design = "Prospective"
        and
        ecmo_mode = "V-V ECMO"
    definition: |-
        A measure of the respiratory rate.


#################################################
blood_products_header:
    section: >-
        Blood Products received {{when}}
#################################################

prbc:
    name: "prbc{{suffix}}"
    label: "PRBC {{when}}"
    type: number
    logic: >-
        study_design = "Prospective"
    definition: |-
        The units of erythrocytes transfused into the patient.

ffp:
    name: "ffp{{suffix}}"
    label: "FFP {{when}}"
    type: number
    logic: >-
        study_design = "Prospective"
    definition: |-
        The units of fresh frozen plasma transfused into the patient.

plt:
    name: "plt{{suffix}}"
    label: "Plt {{when}}"
    type: number
    logic: >-
        study_design = "Prospective"
    definition: |-
        The platelet count is an indicator of patient's clotting ability.

cryop:
    name: "cryop{{suffix}}"
    label: "Cryoprecipitate {{when}}"
    type: number
    logic: >-
        study_design = "Prospective"
    definition: |-
        The units of cryoprecipitate transfused into the patient.


factor_7:
    label: Factor 7?
    radio:
        - 0:  Yes
        - 1:  ~No
    logic: >-
        study_design = "Prospective"
    definition: |-
        Indicates if Factor 7 was used at any point during the ECMO run.

factor_7_dose:
    label: Factor 7 Dose
    logic: >-
        factor_7 = "Yes"
    definition: |-
        Dosage of Factor 7 used (if applicable)

factor_7_initiated:
    label: Factor 7 initiated
    radio:
        - 0:  Before ECMO
        - 1:  On ECMO
    logic: >-
        factor_7 = "Yes" and study_design = "Prospective"
    definition: |-
        Indicates if Factor 7 was administered before or during the ECMO run (if applicable)

pcc:
    label: PCC?
    radio:
        - 0:  Yes
        - 1:  ~No
    logic: >-
        study_design = "Prospective"
    definition: |-
        Indicates if Prothrombin Complex Concentrate was used at any point during the ECMO run.

pcc_dose:
    label: PCC Dose
    logic: >-
        pcc = "Yes"
    definition: |-
        Dosage of PCC used (if applicable)

pcc_initiated:
    label: PCC initiated
    radio:
        - 0:  Before ECMO
        - 1:  On ECMO
    logic: >-
        factor_7 = "Yes" and study_design = "Prospective"
    definition: |-
        Indicates if PCC was administered before or during the ECMO run (if applicable)


#################################################
drugs_header:
    section:  '{{when}} Drugs Used'
#################################################

vasopres:
    name: "vasopres{{suffix}}"
    label: "{{when}} Vasopressors"
    checkbox:
        - 0:  Norepinephrine
        - 1:  Phenylephrine
        - 2:  Vasopressin
        - 3:  ~None
        - 4:  **Other**
    logic: >-
        study_design = "Prospective"
    definition: |-
        The name of the vasopressor(s) used during the specified time period. (if applicable)

inotropes:
    name: "inotropes{{suffix}}"
    label: "{{when}} Inotropes"
    checkbox:
        - 0:  Dobutamine
        - 1:  Epinephrine
        - 2:  Milrinone
        - 3:  ~None
        - 4:  **Other**
    logic: >-
        study_design = "Prospective"
    definition: |-
        The name of the inotrope(s) used during the specified time period. (if applicable)

vasodil:
    label: Vasodilator Used
    checkbox:
        - 0:  Flolan
        - 1:  Nitric Oxide
        - 2:  ~None
        - 3:  **Other**
    definition: |-
        The name of the vasopressor(s) used during ECMO. (if applicable)

antibiotics:
    label: Type of Antibiotic
    checkbox:
        - 0:  Aminoglycosides (gentamycin)
        - 1:  Cephalosporins (cephalexin)
        - 2:  Fluoroquinolones (levofloxacin)
        - 3:  Macrolides (erythromycin, azithromycin)
        - 4:  Penicillins (penicillin, amoxicillin)
        - 5:  Sulfonamides (trimethoprim, co-trimoxazole)
        - 6:  Tetracyclines (tetracycline, doxycycline)
        - 7:  ~None
    definition: |-
        The name of the antibiotics(s) used during the specified time period. (if applicable)

neuroblock:
    label: Neuromuscular blocker
    radio:
        - 0:  Succinylcholine (SCh)
        - 1:  Rocuronium
        - 2:  Vecuronium
        - 3:  Unspecified Neuromuscular Blocking Agent (NMBA)
        - 4:  ~None used
    logic: >-
        ecmo_mode = "V-V ECMO"
        and
        study_design = "Prospective"
    definition: |-
      Indicates the use of neuromuscular blockade during the ECMO run.

sedatives:
    label: Sedatives used
    checkbox:
        - 0:  Barbiturates - Methohexital, Thiopental
        - 1:  Benzodiazepines - Diazepam, Lorazepam, Midazolam
        - 2:  Dexmedetomidine
        - 3:  Ketamine
        - 4:  Neuroleptics -Haloperidol, Olanzapine, Quetiapine, Risperidone
        - 5:  Opioid analgesics -Fentanyl, Hydromorphone, Morphine, Remifentanil
        - 6:  Propofol
        - 7:  Unspecified Sedative
        - 8:  ~None
    logic: >-
        ecmo_mode = "V-V ECMO"
    definition: |-
      The name of the sedative(s) used during the specified time period. (if applicable)

antifibrin:
    label: Antifibrinolytics
    checkbox:
        - 0:  Aminocaproic acid
        - 1:  Tranexemic acid
        - 2:  Unspecified Antifibrinolytic
        - 3:  ~None
    definition: |-
      Indicates the use of anti-fibrinolytics during the ECMO run.


anticoag:
    label: Anticoagulation?
    radio:
        - 0:  Yes
        - 1:  ~No
    definition: |-
        Indicates if any type of therapeutic anticoagulation was used during the ECMO run

anticoag_type:
    label: Type of anticoagulation
    checkbox:
        - 0:  Argatroban drip
        - 1:  Bivalirudin drip
        - 2:  Heparin drip
        - 3:  Lovenox injections
        - 4:  Other
    note: Check if used at any point in ECMO run
    logic: >-
        anticoag = "Yes"
    definition: |-
        Name of therapeutic anticoagulant used (if applicable)

anticoag_init_time:
    label: Anticoagulation Initiation Time
    type: datetime_mdy
    logic: >-
        study_design = "Prospective"
    definition: |-
        Date and time of initiation of anticoagulation.

anticoag_end_time:
    label: Anticoagulation End Time
    type: datetime_mdy
    logic: >-
        study_design = "Prospective"
    definition: |-
        Date and time of discontinuation of anticoagulation.

anticoag_duration:
    label: Anticoagulation Duration
    formula: >-
        datediff([anticoag_init_time],[anticoag_end_time],"h","MDY")
    logic: >-
        study_design = "Prospective"
    definition: |-
        The auto-calculated total duration of anticoagulation.




#################################################


ventilator:
    label: Was the patient ever on a ventilator?
    radio:
        0: 'Yes'
        1: 'No'

ventilator_init_date:
    label: Date of ventilator initiation
    type: date_mdy


ventilator_days:
    label: Days on Ventilator
    type: number
    definition: |-
        The number of days the patient was on ventilator support.


ventilator2:
    name: "ventilator{{suffix}}"
    label: "{{when}} Ventilation?"
    radio:
        - 0:  'Yes'
        - 1:  'No'
    definition: |-
        Indicates whether the patient was placed on a ventilator prior to initiating ECMO.

ventilator_mode:
    name: "ventilator_mode{{suffix}}"
    label: "{{when}} Ventilatory Mode"
    radio:
        - 0:  APRV
        - 1:  High Frequency Oscillatory Ventilation
        - 2:  Pressure Support
        - 3:  Pressure Control
        - 4:  SIMV
        - 5:  Volume Control
        - 6:  **Other**
    logic: >-
        ventilator{{suffix}} = "Yes"
    definition: |-
        Indicates the mode of ventilation prior to initiating ECMO.

pao2:
    name: "pao2{{suffix}}"
    label: "{{when}} PaO2"
    type: number
    definition: Partial Pressure of Arterial Oxygen

fio2:
    name: "fio2{{suffix}}"
    label: "{{when}} FiO2"
    note: "%"
    type: number
    definition: Fraction of Inspired Oxygen

p_f_ratio:
    name: "p_f_ratio{{suffix}}"
    label: "{{when}} PaO2/FiO2 ratio"
    formula: >-
        [pao2{{suffix}}] / [fio2{{suffix}}] * 100
    definition: |-
        An auto-calculated value.
spo2:
    name: "spo2{{suffix}}"
    label: "{{when}} SpO2"
    type: number
    definition: |-
        A measure of the peripheral capillary oxygen saturation.
s_f_ratio:
    name: "s_f_ratio{{suffix}}"
    label: "{{when}} SpO2/FiO2 ratio"
    formula: >-
        [spo2{{suffix}}] / [fio2{{suffix}}] * 100
    definition: "An auto calculated value."

paco2:
    name: "paco2{{suffix}}"
    label: "{{when}} PaCO2"
    type: number
    definition: |-
        Partial Pressure of Arterial Carbon Dioxide

pip:
    name: "pip{{suffix}}"
    label: "{{when}} Peak Inspiratory Pressure (PIP)"
    type: number
    logic: >-
        ecmo_mode = "V-V ECMO"
    definition: |-
        A measure of the highest level of pressure applied to the lungs during mechanical inhalation.

mean_airway_press:
    name: "mean_airway_press{{suffix}}"
    label: "{{when}} Mean Airway Pressure"
    type: number
    logic: >-
        ecmo_mode = "V-V ECMO"
    definition: |-
        A measure of the mean pressure applied during positive-pressure mechanical ventilation.

peep:
    name: "peep{{suffix}}"
    label: "{{when}} PEEP"
    type: number
    logic: >-
        ecmo_mode = "V-V ECMO"
    definition: |-
        Positive End Expiratory Pressure. A measure of the positive pressure provided at the end of expiration during mechanical ventilation.

tidal_vol:
    name: "tidal_vol{{suffix}}"
    label: "{{when}} Tidal Volume"
    type: number
    logic: >-
        ecmo_mode = "V-V ECMO"
    definition: |-
        The volume of air displaced between inhalation and exhalation.

oxidx:
    name: "oxidx{{suffix}}"
    label: "{{when}} Oxygenation Index"
    formula: >-
        0.01 * [fio2{{suffix}}]  * [mean_airway_press{{suffix}}]/ [pao2{{suffix}}]
    logic: >-
        ecmo_mode = "V-V ECMO"
    definition: |-
        The efficiency at which the patient is achieving oxygenation, with lower values being preferred. This is an auto calculated value that uses the formula OI = FiO2 * Mean Airway Pressure/ PaO2.





#################################################


lab_date:
    name: >-
        {{lab_abbrev}}{{suffix}}_date
    label: >-
        Date of {{when}} {{lab}}
    type: date_mdy
    definition: |-
        The date on which the {{when}} value was recorded.

select_header:
    label: Select {{when}} Values
    type: section

ptt_date
-   include: lab_date
    $lab_abbrev: ldh
    $lab: LDH

bun_date
-   include: lab_date
    $lab_abbrev: bun
    $lab: BUN
lactate_date
-   include: lab_date
    $lab_abbrev: lactate
    $lab: Lactate
cre_date
-   include: lab_date
    $lab_abbrev: cre
    $lab: Creatinine
tbili_date
-   include: lab_date
    $lab_abbrev: tbili
    $lab: Total bilirubin
ast_date
-   include: lab_date
    $lab_abbrev: ast
    $lab: AST
alt_date
-   include: lab_date
    $lab_abbrev: alt
    $lab: ALT
ldh_date
-   include: lab_date
    $lab_abbrev: ldh
    $lab: LDH
inr_date
-   include: lab_date
    $lab_abbrev: inr
    $lab: INR
peak_trop_date
-   include: lab_date
    $lab_abbrev: peak_trop
    $suffix: _max
tot_cre_kinase_date
-   include: lab_date
    $lab_abbrev: tot_cre_kinase
    $lab: Total Creatinine kinase
cre_kinase_mb_date
-   include: lab_date
    $lab_abbrev: cre_kinase
    $lab: Creatinine Kinase MB



bmp_date:
    name: "bmp_date{{suffix}}"
    label: "{{when}} BMP Date"
    type: date_mdy
    definition: |-
        The date at which the Basic Metabolic Panel(BMP) was taken.

sodium:
    name: "sodium{{suffix}}"
    label: "{{when}} Sodium"
    note: mEq/L
    type: number
    definition: |-
        A measure of the blood sodium concentration.

ph:
    name: "ph{{suffix}}"
    label: "{{when}} pH"
    type: number
    definition: |-
        A measure of the blood pH.


urineout:
    name: "urineout{{suffix}}"
    label: "{{when}} Urine Output"
    type: number
    definition: |-
        A measure of the urine produced during a 24 hour period.

bun:
    name: "bun{{suffix}}"
    label: "{{when}} BUN"
    note: mg/dL
    type: number
    definition: Blood Urea Nitrogen

bicarb:
    name: "bicarb{{suffix}}"
    label: "{{when}} Bicarbonate"
    type: number
    definition: |-
        A measure of the serum bicarbonate.

lactate:
    name: "lactate{{suffix}}"
    label: "{{when}} Lactate"
    type: number
    definition: |-
        A measure of the serum lactate levels.

cre:
    name: "cre{{suffix}}"
    label: "{{when}} Creatinine"
    note: mg/dL
    type: number
    definition: |-
        A measure of the blood creatinine concentration.

glu:
    name: "glu{{suffix}}"
    label: "{{when}} Glucose"
    type: number
    definition: |-
        A measure of the blood glucose level.

tot_protein:
    name: "tot_protein{{suffix}}"
    label: "{{when}} Total Protein"
    type: number
    definition: |-
        A measure of the total protein.

albumin:
    name: "albumin{{suffix}}"
    label: "{{when}} Alb"
    note: "g/dL"
    type: number
    definition: |-
        A measure of the total serum albumin.

tbili:
    name: "tbili{{suffix}}"
    label: "{{when}} T. Bili"
    note: "mg/dL"
    type: number
    definition: |-
        A measure of the total bilirubin.

ast:
    name: "ast{{suffix}}"
    label: "{{when}} AST"
    note: "u/L"
    type: number
    definition: Aspartate Aminotransferase

alt:
    name: "alt{{suffix}}"
    label: "{{when}} ALT"
    note: "u/L"
    type: number
    definition: Alanine Aminotransferase

alk_phos:
    name: "alk_phos{{suffix}}"
    label: "{{when}} Tot Alk Phos"
    type: number
    definition: Total Alkaline Phosphatase

ldh:
    name: "ldh{{suffix}}"
    label: "{{when}} LDH"
    type: number
    definition: |-
        A measure of the lactate dehydrogenase.

ptt:
    name: "ptt{{suffix}}"
    label: "{{when}} PTT"
    type: number
    definition: |-
        Partial Thromboplastin Time. A measure of the time it takes blood to clot.

inr:
    name: "inr{{suffix}}"
    label: "{{when}} INR"
    note: international units
    type: number
    definition: |-
        International Normalized Ratio. A measure of the time it takes blood to clot.

fibrinogen:
    name: "fibrinogen{{suffix}}"
    label: "{{when}} Fibrinogen"
    type: number
    definition: |-
        The level of fibrinogen, a protein used in clot formation.


peak_trop_max:
    label: Max Peak Trop
    type: number
    definition: |-
        A measure of the Cardiac-specific Troponin I in the blood.

tot_cre_kinase_max:
    label: Max Total CK
    type: number
    definition: |-
        A measure of the total creatinine kinase.

cre_kinase_mb_max:
    label: Max CK-MB
    type: number
    definition: |-
        A measure of the creatinine kinase mb.


#################################################
echo_header:
    section: >-
        {{when}} Echo
#################################################



echo:
    name: "echo{{suffix}}"
    label: "{{when}} Echo?"
    radio:
        - 0:  Yes
        - 1:  ~No
    definition: |-
        Indicates whether an Echocardiogram was taken before ECMO.

echo_date:
    name: "echo{{suffix}}_date"
    label: "{{when}} ECHO: Date"
    type: date_mdy
    definition: |-
        The date of the when the echocardiogram was performed.

lvedd:
    name: "lvedd{{suffix}}"
    label: "{{when}} LVEDD"
    note: mm
    type: number
    logic: >-
        {{#notBlank echo{{suffix}}_date}}
    definition: |-
        A measure of the Left Ventricular End-Diastolic Diameter in the blood.

lvesd:
    name: "lvesd{{suffix}}"
    label: "{{when}} LVESD"
    note: mm
    type: number
    logic: >-
        {{#notBlank echo{{suffix}}_date}}
    definition: |-
        A measure of the Left Ventricular End-Systolic Diameter in the blood.

ef:
    name: "ef{{suffix}}"
    label: "{{when}} EF"
    note: "%"
    type: number
    logic: >-
        {{#notBlank echo{{suffix}}_date}}
    definition: |-
        Ejection Fraction. The percentage of blood emptied from the left ventricle at the end of contraction. If a range was given, then the average was determined and used (i.e., 20-40% = 30%)

ai:
    name: "ai{{suffix}}"
    label: "{{when}} AI"
    radio:
        - 0:  ~None
        - 1:  Trace
        - 2:  Mild
        - 3:  Moderate
        - 4:  Severe
    logic: >-
        {{#notBlank echo{{suffix}}_date}}
    definition: |-
        The level of aortic regurgitation, where
        0 = none, 1 = mild, 2 = moderate, 3 = severe

as:
    name: "as{{suffix}}"
    label: "{{when}} AS"
    radio:
        - 0:  ~None
        - 1:  Trace
        - 2:  Mild
        - 3:  Moderate
        - 4:  Severe
    logic: >-
        {{#notBlank echo{{suffix}}_date}}
    definition: |-
        The level of aortic valve stenosis, where
        0 = none, 1 = mild, 2 = moderate, 3 = severe

mr:
    name: "mr{{suffix}}"
    label: "{{when}} MR"
    radio:
        - 0:  ~None
        - 1:  Trace
        - 2:  Mild
        - 3:  Moderate
        - 4:  Severe
    logic: >-
        {{#notBlank echo{{suffix}}_date}}
    definition: |-
        The level of mitral regurgitation, where
        0 = none, 1 = mild, 2 = moderate, 3 = severe

ms:
    name: "ms{{suffix}}"
    label: "{{when}} MS"
    radio:
        - 0:  ~None
        - 1:  Trace
        - 2:  Mild
        - 3:  Moderate
        - 4:  Severe
    logic: >-
        {{#notBlank echo{{suffix}}_date}}
    definition: |-
        The level of mitral valve stenosis, where
        0 = none, 1 = mild, 2 = moderate, 3 = severe

tr:
    name: "tr{{suffix}}"
    label: "{{when}} TR"
    radio:
        - 0:  ~None
        - 1:  Trace
        - 2:  Mild
        - 3:  Moderate
        - 4:  Severe
    logic: >-
        {{#notBlank echo{{suffix}}_date}}
    definition: |-
        Indicates the level of Tricuspid valve regurgitation, where
        0 = none, 1 = mild, 2 = moderate, 3 = severe


serial_echos_post:
    label: Serial Echos?
    radio:
        - 0:  Yes
        - 1:  ~No
    logic: >-
        {{#notBlank echo_post_date}}
    definition: |-
        Indicates whether serial echos were taken.


#################################################

iabp:
    name: "iabp{{suffix}}"
    label: "{{when}} IABP?"
    radio:
        - 0:  Yes
        - 1:  ~No
    definition: Intra-aortic Ballon Pump



cath_date:
    name: "cath_date{{suffix}}"
    label: "{{when}} Cath: Date"
    type: date_mdy
    definition: |-
        The date of catheterization, prior to ECMO.


rhythm:
    name: "rhythm{{suffix}}"
    label: "{{when}} Cardiac Rhythm"
    type: number
    definition: |-
        The cardiac rhythm.

sbp:
    name: "sbp{{suffix}}"
    label: "{{when}} SBP"
    note: mmHg
    type: number
    definition: |-
        The systolic blood pressure, in mmHg.

dbp:
    name: "dbp{{suffix}}"
    label: "{{when}} DBP"
    note: mmHg
    type: number
    definition: |-
        The diastolic blood pressure, in mmHg.

pulm_sbp:
    name: "pulm_sbp{{suffix}}"
    label: "{{when}} PA SBP"
    note: mmHg
    type: number
    definition: |-
        The systolic blood pressure in the pulmonary artery, in mmHg.

pulm_dbp:
    name: "pulm_dbp{{suffix}}"
    label: "{{when}} PA DBP"
    note: mmHg
    type: number
    definition: |-
        The diastolic blood pressure in the pulmonary artery, in mmHg.

pulm_map:
    name: "pulm_map{{suffix}}"
    label: "{{when}} PA MAP"
    note: mmHg
    type: number
    logic: >-
        study_design = "Prospective"
    definition: |-
        The mean arterial pressure in the pulmonary artery, in mmHg.

pcwp:
    name: "pcwp{{suffix}}"
    label: "{{when}} PCWP"
    note: mmHg
    type: number
    logic: >-
        study_design = "Prospective"
    definition: |-
        The pulmonary capillary wedge pressure, in mmHg.

cvp_rap:
    name: "cvp_rap{{suffix}}"
    label: "{{when}} CVP/RAP"
    note: mmHg
    type: number
    logic: >-
        study_design = "Prospective"
    definition: |-
        The central venous pressure/right atrial pressure, in mmHg.

svo2:
    name: "svo2{{suffix}}"
    label: "{{when}} SVO2"
    type: number
    logic: >-
        study_design = "Prospective"
    definition: Mixed Venous Oxygen Saturation

co:
    name: "co{{suffix}}"
    label: "{{when}} CO"
    note: L/min
    type: number
    logic: >-
        study_design = "Prospective"
        Cardiac Output, in L/min.
    definition: |-
      undefined





special_labs_header:
  label: Special Labs
  type: section


hit_antibody:
    label: Positive Heparin-PF4 Antibody?
    type: radio
    options:
        - 0:  Yes
        - 1:  ~No
    definition: |-
        A positive result indicates the presence of serum antibodies to H/PF4.

sra:
    label: Serotonin release assay
    type: radio
    options:
        - 0:  Negative
        - 1:  Positive
        - 2:  ~Not done
    logic: >-
        hit_antibody = "Yes"
    definition: |-
        A positive result indicates the presence of serum antibodies to H/PF4. This test is a better predictor of thrombosis than the anti-PF4 assay.

bnp:
    label: BNP
    type: number
    definition: |-
        A measure of the B-type Natriuretic Peptide in the blood.

